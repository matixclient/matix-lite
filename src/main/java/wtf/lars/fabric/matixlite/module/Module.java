package wtf.lars.fabric.matixlite.module;

public class Module {

    private final String name;
    private final ModuleCategory category;
    private int key;
    private boolean enabled;

    public Module(String name, ModuleCategory category, int key) {
        this.name = name;
        this.category = category;
        this.key = key;
    }

    public String getName() {
        return this.name;
    }

    public ModuleCategory getCategory() {
        return this.category;
    }

    public int getKey() {
        return this.key;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void toggle() {
        this.setEnabled(!this.enabled);
    }

}
