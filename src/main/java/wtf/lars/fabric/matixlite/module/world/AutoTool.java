package wtf.lars.fabric.matixlite.module.world;

import net.java.games.input.Keyboard;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SwordItem;
import net.minecraft.util.math.BlockPos;
import wtf.lars.fabric.matixlite.MatixLite;
import wtf.lars.fabric.matixlite.Wrapper;
import wtf.lars.fabric.matixlite.module.Module;
import wtf.lars.fabric.matixlite.module.ModuleCategory;

public class AutoTool extends Module {

    public AutoTool() {
        super("AutoTool", ModuleCategory.PLAYER, 72); // H
    }

    public static void handleBlockClick(BlockPos blockPos) {
        if (MatixLite.getInstance().getModuleManager().isEnabled("AutoTool")) {
            AutoTool.autoTool(blockPos);
        }
    }

    private static void autoTool(BlockPos blockPos) {
        BlockState blockState = Wrapper.getWorld().getBlockState(blockPos);

        if (blockState.getMaterial() != Material.AIR) {
            float s = 0.1F;
            int currentItem = Wrapper.getPlayer().inventory.selectedSlot;

            for (int inventoryIndex = 36; inventoryIndex < 45; ++inventoryIndex) {
                ItemStack is = Wrapper.getPlayer().container.getSlot(inventoryIndex).getStack();

                if (is != null) {
                    if (Wrapper.getPlayer().isCreative() && is.getItem() instanceof SwordItem) {
                        continue;
                    }

                    float strength = AutoTool.getPlayerStrVsBlock(blockState, is);

                    if (strength > s) {
                        s = strength;
                        currentItem = inventoryIndex - 36;
                    }
                }
            }

            Wrapper.getPlayer().inventory.selectedSlot = currentItem;
        }
    }

    private static float getPlayerStrVsBlock(BlockState blockState, ItemStack itemStack) {
        float strength = 1.0F;

        if (itemStack != null && blockState != null) {
            strength *= itemStack.getMiningSpeed(blockState);
        }

        return strength;
    }

}
