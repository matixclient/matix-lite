package wtf.lars.fabric.matixlite.module;

import wtf.lars.fabric.matixlite.module.world.AutoTool;

import java.util.concurrent.ConcurrentHashMap;

public class ModuleManager {

    private final ConcurrentHashMap<String, Module> moduleList = new ConcurrentHashMap<>();

    public ModuleManager() {
        this.loadModules();
    }

    private void loadModules() {
        this.addModule(new AutoTool());
    }

    public void addModule(Module module) {
        this.moduleList.put(module.getName(), module);
    }

    public void handleKeyPress(int keyCode) {
        this.moduleList.values().stream().filter(module -> module.getKey() == keyCode).forEach(Module::toggle);
    }

    public boolean isEnabled(String moduleName) {
        return this.moduleList.values().stream().filter(module -> module.getName().equals(moduleName)).anyMatch(Module::isEnabled);
    }

}
