package wtf.lars.fabric.matixlite.module;

public enum ModuleCategory {
    PLAYER,
    WORLD,
    COMBAT,
    RENDER,
    MOVEMENT,
    OTHER;

    @Override
    public String toString() {
        String curString = super.toString();
        return curString.charAt(0) + curString.substring(1).toLowerCase();
    }

    public long getColor() {
        switch (this) {
            case PLAYER:
                return 0xD31EC2L;
            case WORLD:
                return 0x1EC0A8;
            case COMBAT:
                return 0xBB0711L;
            case RENDER:
                return 0xA3F330L;
            case MOVEMENT:
                return 0xDEA225L;
            case OTHER:
                return 0x239991L;
        }

        return 0;
    }
}