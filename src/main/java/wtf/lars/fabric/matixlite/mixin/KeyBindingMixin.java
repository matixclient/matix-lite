package wtf.lars.fabric.matixlite.mixin;

import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil.KeyCode;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import wtf.lars.fabric.matixlite.MatixLite;

@Mixin(KeyBinding.class)
public class KeyBindingMixin {

    @Inject(at = @At("HEAD"), method = "onKeyPressed")
    private static void onKeyPressed(KeyCode inputUtil$KeyCode_1, CallbackInfo callbackInfo) {
        MatixLite.getInstance().getModuleManager().handleKeyPress(inputUtil$KeyCode_1.getKeyCode());
        System.out.println("Key Pressed: " + inputUtil$KeyCode_1.getKeyCode());
    }

}
