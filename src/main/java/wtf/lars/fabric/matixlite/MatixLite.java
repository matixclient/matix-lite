package wtf.lars.fabric.matixlite;

import net.fabricmc.api.ModInitializer;
import wtf.lars.fabric.matixlite.module.ModuleManager;

public class MatixLite implements ModInitializer {

	private static MatixLite instance;

	private ModuleManager moduleManager;

	@Override
	public void onInitialize() {
		MatixLite.instance = this;

		this.moduleManager = new ModuleManager();
	}

	public static MatixLite getInstance() {
		return MatixLite.instance;
	}

	public ModuleManager getModuleManager() {
		return this.moduleManager;
	}
}
