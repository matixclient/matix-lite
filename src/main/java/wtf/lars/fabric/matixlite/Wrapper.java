package wtf.lars.fabric.matixlite;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.world.World;

public class Wrapper {

    public static MinecraftClient getMinecraft() {
        return MinecraftClient.getInstance();
    }

    public static World getWorld() {
        return Wrapper.getMinecraft().world;
    }

    public static ClientPlayerEntity getPlayer() {
        return Wrapper.getMinecraft().player;
    }

}
